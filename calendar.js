$(document).ready(function () {
  var data = [
    {
      category_name: "S13",
      product_code: "3162",
      product_name: "機材名1",
      product_number: "2345",
      product_note: "メモ1",
      reserve: [
        {
          start: "2021-08-01",
          end: "2021-08-05",
        },
        {
          start: "2021-08-05",
          end: "2021-08-06",
        },
      ],
      export: [
        {
          start: "2021-08-04",
          end: "2021-08-10",
        },
      ],
      repair: [
        {
          start: "2021-08-11",
          end: "2021-08-13",
        },
      ],
    },
    {
      category_name: "推進",
      product_code: "000000201",
      product_name: "アンクルモール V 　TCV-200(Ⅰ)",
      product_number: "2912TCV015",
      product_note: "メモ2",
      reserve: [
        {
          start: "2021-08-01",
          end: "2021-08-06",
        },
      ],
      export: [
        {
          start: "2021-08-07",
          end: "2021-08-10",
        },
        {
          start: "2021-08-11",
          end: "2021-08-17",
        },
      ],
      repair: [
        {
          start: "2021-08-18",
          end: "2021-08-21",
        },
      ],
    },
    {
      category_name: "S13",
      product_code: "3162",
      product_name: "機材名2",
      product_number: "2345",
      product_note: "メモ1",
      reserve: [
        {
          start: "2021-12-01",
          end: "2021-12-05",
        },
        {
          start: "2021-01-05",
          end: "2021-01-06",
        },
      ],
      export: [
        {
          start: "2021-12-04",
          end: "2021-12-10",
        },
      ],
      repair: [
        {
          start: "2021-1-11",
          end: "2021-1-13",
        },
      ],
    },
    {
      category_name: "S13",
      product_code: "3162",
      product_name: "機材名3",
      product_number: "2345",
      product_note: "メモ1",
      reserve: [
        {
          start: "2021-12-01",
          end: "2021-12-05",
        },
        {
          start: "2021-01-05",
          end: "2021-01-06",
        },
      ],
      export: [
        {
          start: "2021-12-04",
          end: "2021-12-10",
        },
      ],
      repair: [
        {
          start: "2021-1-11",
          end: "2021-1-13",
        },
      ],
    },
    {
      category_name: "S13",
      product_code: "3162",
      product_name: "機材名4",
      product_number: "2345",
      product_note: "メモ1",
      reserve: [
        {
          start: "2021-12-01",
          end: "2021-12-05",
        },
        {
          start: "2021-01-05",
          end: "2021-01-06",
        },
      ],
      export: [
        {
          start: "2021-12-04",
          end: "2021-12-10",
        },
      ],
      repair: [
        {
          start: "2021-1-11",
          end: "2021-1-13",
        },
      ],
    },
    {
      category_name: "S13",
      product_code: "3162",
      product_name: "機材名5",
      product_number: "2345",
      product_note: "メモ1",
      reserve: [
        {
          start: "2021-12-01",
          end: "2021-12-05",
        },
        {
          start: "2021-01-05",
          end: "2021-01-06",
        },
      ],
      export: [
        {
          start: "2021-12-04",
          end: "2021-12-10",
        },
      ],
      repair: [
        {
          start: "2021-1-11",
          end: "2021-1-13",
        },
      ],
    },
    {
      category_name: "S13",
      product_code: "3162",
      product_name: "機材名6",
      product_number: "2345",
      product_note: "メモ1",
      reserve: [
        {
          start: "2021-12-01",
          end: "2021-12-05",
        },
        {
          start: "2021-01-05",
          end: "2021-01-06",
        },
      ],
      export: [
        {
          start: "2021-12-04",
          end: "2021-12-10",
        },
      ],
      repair: [
        {
          start: "2021-1-11",
          end: "2021-1-13",
        },
      ],
    },
    {
      category_name: "S13",
      product_code: "3162",
      product_name: "機材名7",
      product_number: "2345",
      product_note: "メモ1",
      reserve: [
        {
          start: "2021-12-01",
          end: "2021-12-05",
        },
        {
          start: "2021-01-05",
          end: "2021-01-06",
        },
      ],
      export: [
        {
          start: "2021-12-04",
          end: "2021-12-10",
        },
      ],
      repair: [
        {
          start: "2021-1-11",
          end: "2021-1-13",
        },
      ],
    },
    {
      category_name: "S13",
      product_code: "3162",
      product_name:
        "アンクルモール V 　TCV-200(Ⅰ)　2　小口径　掘進機　泥水　塩ビ管",
      product_number: "2345",
      product_note: "メモ1",
      reserve: [
        {
          start: "2021-08-01",
          end: "2021-08-05",
        },
        {
          start: "2021-01-05",
          end: "2021-01-06",
        },
      ],
      export: [
        {
          start: "2021-08-04",
          end: "2021-08-10",
        },
      ],
      repair: [
        {
          start: "2021-1-11",
          end: "2021-1-13",
        },
      ],
    },
  ];

  // buildCalendar("#calendar-body", data);

  initPagination({
    container: ".calendar-paginate",
    data: data,
    pageSize: 4,
  });
  $("#modalBooking").data("list_product", checkedData);
  $(".open-booking-modal").on("click", showBookingModal);
});

var checkedData = JSON.parse(localStorage.getItem("userId")) || [];

function buildCalendar(container, data) {
  var currentDate = new Date();
  var currentYear = currentDate.getFullYear();
  var currentMonth = currentDate.getMonth();
  $(".hd-date").html(
    currentDate.getFullYear() + "年" + (currentDate.getMonth() + 1) + "月"
  );
  var calendar = buildTable(currentYear, currentMonth, data);

  calendarContainer = document.querySelector(container);
  calendarInner = newElement("div");
  calendarInner.className = "calendar-inner";
  calendarInner.appendChild(calendar);
  calendarContainer.appendChild(calendarInner);
  $(".calendar-table").clone(true).appendTo(container).addClass("clone");

  calcLayout();
  controlSelector = $(".hd-btn");
  controlSelector.on("click", calendarControl);

  ///// Build calendar table /////
  function buildTable(year, month, data) {
    var table = newElement("table");
    var thead = newElement("thead");
    var tbody = newElement("tbody");
    var firstTrThead = newElement("tr");
    var secondTrThead = newElement("tr");

    firstTrThead.innerHTML =
      '<th class="product-header first-col" rowspan="2">選択／工種</th>' +
      '<th class="product-header second-col" rowspan="2">機材名</th>' +
      '<th class="product-header third-col" rowspan="2">メモ</th>';

    var dayOfCurrentMonth = new Date(year, month + 1, 0);
    var dayOfNextMonth = new Date(year, month + 2, 0);
    var allDay = dayOfCurrentMonth.getDate() + dayOfNextMonth.getDate();

    newMonthCell(year, month, firstTrThead);

    // Call build day cells
    for (i = 1; i <= allDay; i++) {
      th = newDayCell(new Date(year, month, i));
      secondTrThead.appendChild(th);
    }

    // Call build tbody
    for (d = 0; d < data.length; d++) {
      var trBody = newElement("tr");
      trBody.innerHTML = productsRow(data[d]);

      for (i = 1; i <= allDay; i++) {
        td = newEventCell(new Date(year, month, i), data[d]);
        trBody.appendChild(td);
      }

      tbody.appendChild(trBody);
    }

    thead.appendChild(firstTrThead);
    thead.appendChild(secondTrThead);
    table.appendChild(thead);
    table.appendChild(tbody);
    table.setAttribute("cellpadding", "0");
    table.className = "calendar-table";

    addEventListener(".badge", "click", showBookingModal);
    removeEventListener(
      ".calendar-table input[type='checkbox']",
      "change",
      getCheckedData
    );
    addEventListener(
      ".calendar-table input[type='checkbox']",
      "change",
      getCheckedData
    );

    return table;
  }

  ///// Create new month cell /////
  function newMonthCell(year, month, thead) {
    for (i = 1; i <= 2; i++) {
      th = newElement("th");
      current = new Date(year, month + i, 0);

      th.innerHTML =
        current.getFullYear() + "年" + (current.getMonth() + 1) + "月";
      th.className = "thead-week";
      th.setAttribute("colspan", current.getDate());
      thead.appendChild(th);
    }
  }

  ///// Create new day cell /////
  function newDayCell(date) {
    switch (date.getDay()) {
      case 0:
        day = "日";
        break;
      case 1:
        day = "月";
        break;
      case 2:
        day = "火";
        break;
      case 3:
        day = "水";
        break;
      case 4:
        day = "木";
        break;
      case 5:
        day = "金";
        break;
      case 6:
        day = "土";
        break;
      default:
        day = "日";
    }

    th = newElement("th");
    isoDate = moment(date).format("yyyy-MM-DD");
    th.className = "day thead-date";
    th.setAttribute("data-date", isoDate);

    th.innerHTML = day + "<br/>" + date.getDate();

    return th;
  }

  ///// Build product rows with data /////
  function productsRow(data) {
    var category = data.category_name;
    var productName = data.product_name;
    var productCode = data.product_code;
    var productNote = data.product_note;
    var productNumber = data.product_number;
    var isChecked = checkedData.includes(productName) ? "checked" : "";

    return (
      '<th class="product-info first-col"><div class="product-cont"><label class="custom-checkbox avoid-click"><input type="checkbox"' +
      isChecked +
      '><span class="checkmark"></span><span>' +
      category +
      "</span></label></div></th>" +
      '<th class="product-info second-col"><div class="product-cont">' +
      '<div class="product-name">' +
      productName +
      '</div><div class="product-code">' +
      productCode +
      '</div></div></th><th class="product-info third-col"><div class="product-cont"><div class="product-note">' +
      productNote +
      '</div><div class="product-number">' +
      productNumber +
      "</div></div></th>"
    );
  }

  ///// Build product cell with data event /////
  function newEventCell(date, data) {
    var td = newElement("td");
    var isoDate = moment(date).format("yyyy-MM-DD");

    td.className =
      date.getDay() == 0 || date.getDay() == 6
        ? "event-cell weekend"
        : "event-cell";
    td.setAttribute("data-date", isoDate);

    if (data.reserve) {
      data.reserve.map((reser) => {
        if (isoDate >= reser.start && isoDate <= reser.end) {
          badge = newElement("span");
          badge.className = "badge badge-reserve";
          badge.setAttribute("data-type", "reserve");
          td.appendChild(badge);
        }
      });
    }

    if (data.export) {
      data.export.map((exp) => {
        if (isoDate >= exp.start && isoDate <= exp.end) {
          badge = newElement("span");
          badge.className = "badge badge-export";
          badge.setAttribute("data-type", "export");
          td.appendChild(badge);
        }
      });
    }

    if (data.repair) {
      data.repair.map((rep) => {
        if (isoDate >= rep.start && isoDate <= rep.end) {
          badge = newElement("span");
          badge.className = "badge badge-repair";
          badge.setAttribute("data-type", "repair");
          td.appendChild(badge);
        }
      });
    }

    return td;
  }

  ///// Create new html element /////
  function newElement(tagName) {
    return document.createElement(tagName);
  }

  ///// Get new day /////
  function addDays(date, days) {
    const copy = new Date(Number(date));
    copy.setDate(date.getDate() + days);
    return copy;
  }

  ///// Calendar control /////
  function calendarControl(event) {
    var action = $(this).data("calendar-control");

    switch (action) {
      case "prev":
        currentDate.setMonth(currentDate.getMonth() - 1);
        break;
      case "next":
        currentDate.setMonth(currentDate.getMonth() + 1);
        break;
    }

    calendar = buildTable(
      currentDate.getFullYear(),
      currentDate.getMonth(),
      data
    );
    $(".clone").remove();
    calendarInner.removeChild(calendarInner.firstChild);
    calendarInner.insertBefore(calendar, calendarInner.firstChild);
    $(".calendar-table").clone(true).appendTo(container).addClass("clone");
    calcLayout();

    console.log("Change month");

    $(".hd-date").html(
      currentDate.getFullYear() + "年" + (currentDate.getMonth() + 1) + "月"
    );
  }

  ///// Add Event Listener for new html element
  function addEventListener(target, event, handler) {
    $(document).on(event, target, handler);
  }

  ///// Remove Event Listener for new html element
  function removeEventListener(target, event, handler) {
    $(document).off(event, target, handler);
  }

  ///// Calculate layout table calendar /////
  function calcLayout() {
    sumWidthPrevious = 0;
    calcWidthSelector = [".first-col", ".second-col", ".third-col"];
    calcWidthSelector.map((selector, index) => {
      $(selector).css("left", sumWidthPrevious);
      sumWidthPrevious += $(selector).outerWidth();
    });
  }
}

function initPagination(option) {
  var container = document.querySelector(option.container);
  var currentPage = option.currentPage || 1;
  var data = option.data;
  var pageSize = option.pageSize || 10;
  var pageRange = option.pageRange || 2;

  var totalPage = Math.ceil(data.length / pageSize);

  var listPage = buildListPage({
    currentPage: currentPage,
    pageRange: pageRange,
    totalPage: totalPage,
  });

  ///// Clear data /////
  $(container).empty();

  selectPage(currentPage);
  container.appendChild(listPage);

  ///// Slice data for page /////
  function dataForPage(page) {
    return data.slice((page - 1) * pageSize, (page - 1) * pageSize + pageSize);
  }

  ///// Select page /////
  function selectPage(page) {
    currentPage = page;
    var data = dataForPage(page);
    $(".calendar-body").empty();
    $(container).empty();
    listPage = buildListPage({
      currentPage: currentPage,
      pageRange: pageRange,
      totalPage: totalPage,
    });
    container.appendChild(listPage);
    buildCalendar(".calendar-body", data);
  }

  function buildListPage(option) {
    var currentPage = option.currentPage || 1;
    var totalPage = option.totalPage;

    var pagyContainer = newElement("div");
    pagyContainer.className = "pagination mt-2";

    var pagyInner = newElement("div");
    pagyInner.className = "pagination-page";
    pagyContainer.appendChild(pagyInner);

    var listPage = newElement("ul");
    var previous = newElement("li");
    prevLink = newElement("a");
    prevLink.innerHTML = "&laquo;";

    if (currentPage <= 1) {
      previous.className = "pagination-prev disabled";
    } else {
      previous.className = "pagination-prev";
      previous.setAttribute("data-num", currentPage - 1);
      prevLink.setAttribute("href", "");
      prevLink.addEventListener("click", function (e) {
        e.preventDefault();
        console.log("select page " + (currentPage - 1));
        selectPage(currentPage - 1);
      });
    }

    previous.appendChild(prevLink);
    listPage.appendChild(previous);

    option.listPage = listPage;
    buildPageNumber(option);

    var next = newElement("li");
    nextLink = newElement("a");
    nextLink.innerHTML = "&raquo;";

    if (currentPage >= totalPage) {
      next.className = "pagination-next disabled";
    } else {
      next.className = "pagination-next";
      next.setAttribute("data-num", currentPage + 1);
      nextLink.setAttribute("href", "");
      nextLink.addEventListener("click", function (e) {
        e.preventDefault();
        console.log("select page " + (currentPage + 1));
        selectPage(currentPage + 1);
      });
    }

    next.appendChild(nextLink);
    listPage.appendChild(next);
    pagyInner.appendChild(listPage);

    return pagyContainer;
  }

  function buildPageNumber(option) {
    var listPage = option.listPage;
    var currentPage = option.currentPage;
    var totalPage = option.totalPage;
    var pageRange = option.pageRange;
    var i;

    var ellipsisText = "...";
    var rangeStart = currentPage - pageRange;
    var rangeEnd = currentPage + pageRange;

    if (rangeEnd > totalPage) {
      rangeEnd = totalPage;
      rangeStart = totalPage - pageRange * 2;
      rangeStart = rangeStart < 1 ? 1 : rangeStart;
    }

    if (rangeStart <= 1) {
      rangeStart = 1;
      rangeEnd = Math.min(pageRange * 2 + 1, totalPage);
    }

    ///// Show all pages /////
    if (pageRange === null) {
      for (i = 1; i <= totalPage; i++) {
        var pageElement = newElement("li");
        var pageLink = newElement("a");
        pageElement.setAttribute("data-num", i);
        pageElement.classList.add("page_" + i);
        pageLink.innerText = i;

        if (i == currentPage) {
          pageElement.className = "pagination-page active";
        } else {
          pageElement.className = "pagination-page";
          pageLink.setAttribute("href", "");
          pageLink.addEventListener("click", function (e) {
            e.preventDefault();
            page = $(this).parent().data("num");
            console.log("select page " + page);
            selectPage(page);
          });
        }

        pageElement.appendChild(pageLink);
        listPage.appendChild(pageElement);
        return;
      }
    }

    if (rangeStart <= 3) {
      for (i = 1; i < rangeStart; i++) {
        var pageElement = newElement("li");
        var pageLink = newElement("a");
        pageElement.setAttribute("data-num", i);
        pageElement.classList.add("page_" + i);
        pageLink.innerText = i;

        if (i == currentPage) {
          pageElement.className = "pagination-page active";
        } else {
          pageElement.className = "pagination-page";
          pageLink.setAttribute("href", "");
          pageLink.addEventListener("click", function (e) {
            e.preventDefault();
            page = $(this).parent().data("num");
            console.log("select page " + page);
            selectPage(page);
          });
        }

        pageElement.appendChild(pageLink);
        listPage.appendChild(pageElement);
      }
    } else {
      var firstPage = newElement("li");
      var firstLink = newElement("a");
      firstPage.className = "pagination-page";
      firstPage.classList.add("page_" + i);
      firstPage.setAttribute("data-num", 1);
      firstLink.setAttribute("href", "");
      firstLink.innerText = 1;
      firstLink.addEventListener("click", function (e) {
        e.preventDefault();
        console.log("select page " + 1);
        selectPage(1);
      });
      firstPage.appendChild(firstLink);
      listPage.appendChild(firstPage);

      var ellipsis = newElement("li");
      var ellipsisLink = newElement("a");
      ellipsis.className = "pagination-ellipsis disabled";
      ellipsisLink.innerText = ellipsisText;
      ellipsis.appendChild(ellipsisLink);
      listPage.appendChild(ellipsis);
    }

    for (i = rangeStart; i <= rangeEnd; i++) {
      var pageElement = newElement("li");
      var pageLink = newElement("a");
      pageElement.setAttribute("data-num", i);
      pageElement.classList.add("page_" + i);
      pageLink.innerText = i;

      if (i == currentPage) {
        pageElement.className = "pagination-page active";
      } else {
        pageElement.className = "pagination-page";
        pageElement.classList.add("page_" + i);
        pageLink.setAttribute("href", "");
        pageLink.addEventListener("click", function (e) {
          e.preventDefault();
          page = $(this).parent().data("num");
          console.log("select page " + page);
          selectPage(page);
        });
      }

      pageElement.appendChild(pageLink);
      listPage.appendChild(pageElement);
    }

    if (rangeEnd >= totalPage - 2) {
      for (i = rangeEnd + 1; i <= totalPage; i++) {
        var pageElement = newElement("li");
        var pageLink = newElement("a");
        pageElement.setAttribute("data-num", i);
        pageElement.classList.add("page_" + i);
        pageLink.innerText = i;

        if (i == currentPage) {
          pageElement.className = "pagination-page active";
        } else {
          pageElement.className = "pagination-page";
          pageLink.setAttribute("href", "");
          pageLink.addEventListener("click", function (e) {
            e.preventDefault();
            page = $(this).parent().data("num");
            console.log("select page " + page);
            selectPage(page);
          });
        }

        pageElement.appendChild(pageLink);
        listPage.appendChild(pageElement);
      }
    } else {
      var ellipsis = newElement("li");
      var ellipsisLink = newElement("a");
      ellipsis.className = "pagination-ellipsis disabled";
      ellipsisLink.innerText = ellipsisText;
      ellipsis.appendChild(ellipsisLink);
      listPage.appendChild(ellipsis);

      var lastPage = newElement("li");
      var lastLink = newElement("a");
      lastPage.className = "pagination-page";
      lastPage.setAttribute("data-num", totalPage);
      lastLink.setAttribute("href", "");
      lastLink.addEventListener("click", function (e) {
        e.preventDefault();
        pageElement.classList.add("page_" + totalPage);
        console.log("select page " + totalPage);
        selectPage(totalPage);
      });
      lastLink.innerText = totalPage;
      lastPage.appendChild(lastLink);
      listPage.appendChild(lastPage);
    }
  }
}

function getCheckedData(e) {
  var targetValue = $(e.target).closest("tr").find(".product-name").text();

  if ($(e.target).hasClass("badge")) {
    checkedData.push($(e.target).closest("tr").find(".product-name").text());
  } else {
    if (e.target.checked) {
      checkedData.push(targetValue);
    } else {
      checkedData = arrayRemove(checkedData, targetValue);
    }
  }
  console.log("data", checkedData);
  $("#modalBooking").data("list_product", checkedData);
  localStorage.setItem("userId", JSON.stringify(checkedData));
}

///// Show modal /////
function showBookingModal(e) {
  var listProduct = $("#modalBooking").data("list_product");

  if ($(e.target).hasClass("btn-edit-booking")) {
    var selector = $('#table-body input[type="checkbox"]:checked');
    var actionType = "edit";
  } else {
    var selector = $('#calendar-body input[type="checkbox"]:checked');
    var actionType = "create";
  }

  $("#modalBooking").attr("action-type", actionType);

  if (["reserve", "export"].includes($(e.target).data("type"))) {
    $("#modalBooking").attr("type", $(e.target).data("type"));
  }
  if ($(e.target).hasClass("open-booking-modal"))
    $("#modalBooking").attr("type", "reserve");

  if (listProduct === undefined || !listProduct.length) {
    $("#modalWarning").modal("show");
  } else {
    $("#modalBooking").modal("show");
  }
}

///// Remove element in array /////,
function arrayRemove(arr, value) {
  return arr.filter(function (ele) {
    return ele != value;
  });
}

///// Handle escape keyboard hide modal /////
$(document).keyup(function (e) {
  if (e.key === "Escape") {
    if ($("#modalWarning").hasClass("show")) {
      $("#modalWarning").modal("hide");
    } else if ($(".modal").hasClass("show")) {
      Rails.fire($(".closefirstmodal")[0], "click");
    }
  }
});

///// Handle confirm close modal /////
$(document).on("click", ".confirmclosed", function () {
  window.has_form_changed = false;
  $("#modalWarning").modal("hide");
  return $("#remoteModal, #remoteModalLg, #remoteModalXl").modal("hide");
});
