$(document).ready(function () {
  $(".btn-new").on("click", function (e) {
    var newItem =
      '<div class="content multiple-form">' +
      '<div class="form-index">機材②</div>' +
      '<form class="product-form">' +
      '<div class="form-wrapper">' +
      '<div class="form-header">' +
      '<div class="form-title">予約エリア</div>' +
      "</div>" +
      '<div class="form-content">' +
      '<div class="row">' +
      '<div class="col-md-6 group-input">' +
      '<div class="row">' +
      '<div class="col-md-6 group-input">' +
      "<label>日付</label>" +
      '<input type="text" name="" placeholder="9 ~ 10">' +
      "</div>" +
      '<div class="col-md-6 group-input">' +
      "<label>担当者</label>" +
      '<input type="text" name="" placeholder="Test">' +
      "</div>" +
      "</div>" +
      "</div>" +
      '<div class="col-md-6">' +
      '<div class="row">' +
      '<div class="col-md-6 group-input">' +
      "<label>現場コード</label>" +
      '<input type="text" name="" placeholder="150">' +
      "</div>" +
      '<div class="col-md-6 group-input">' +
      "<label>現場名</label>" +
      '<input type="text" name="" placeholder="HN">' +
      "</div>" +
      "</div>" +
      "</div>" +
      "</div>" +
      "</div>" +
      "</div>" +
      '<div class="form-action">' +
      '<button class="btn btn-form">キャンセル</button>' +
      '<button class="btn btn-form">OK</button>' +
      "</div>" +
      "</form>" +
      "</div>";
    $(".modal-body").append(newItem);
    $(".content").addClass("multiple-form");
    $(".group-input").addClass("col-md-6");
    $(".group-input").removeClass("col-md-12 mb-3");
  });

  $("#modalBooking").on("hidden.bs.modal", function (e) {
    $(".content:not(:first)").remove();
    $(".content").removeClass("multiple-form");
    $(".group-input").removeClass("col-md-6");
    $(".group-input").addClass("col-md-12");
    $(".first-row").addClass("mb-3");
  });
});
