$(document).ready(function () {
  $("#modalBooking").on("show.bs.modal", function (e) {
    var listProduct = $("#modalBooking").data("list_product");
    console.log(listProduct);
    var type = $("#modalBooking").attr("type");

    if (type == "reserve") {
      $(".label-type").removeClass("btn-export");
      $(".label-type").addClass("btn-reserve");
    } else {
      $(".label-type").removeClass("btn-reserve");
      $(".label-type").addClass("btn-export");
    }

    if (listProduct.length > 0) {
      for (i = 0; i < listProduct.length; i++) {
        buildForm(listProduct[i]);
      }
      $(".btn-remove-item").on("click", removeItem);
    }
  });

  $("#modalBooking").on("hidden.bs.modal", function (e) {
    $(".content").remove();
  });
});

function buildForm(
  product,
  start,
  end,
  code,
  name,
  quantity,
  billType,
  billId,
  productId,
  fixQuantity,
  constructions
) {
  var contentDiv = newElement("div");
  contentDiv.className = "content";
  $(contentDiv).data("item", product);
  $(contentDiv).data("billId", billId);

  var titleDiv = newElement("div");
  titleDiv.className =
    "title d-flex flex-row justify-content-between align-items-center";
  contentDiv.appendChild(titleDiv);

  var productNameDiv = newElement("div");
  productNameDiv.className = "form-product-name";
  productNameDiv.innerText = "予約機材: " + product;
  titleDiv.appendChild(productNameDiv);

  var removeItemBtn = newElement("button");
  removeItemBtn.className = "btn btn-secondary m-0 px-4 py-2 btn-remove-item";
  removeItemBtn.innerText = "削除";
  titleDiv.appendChild(removeItemBtn);

  var newForm = newElement("form");
  newForm.className = "product-form";
  contentDiv.appendChild(newForm);

  var formWrapper = newElement("div");
  formWrapper.className = "form-wrapper";
  newForm.appendChild(formWrapper);

  var formHeader = newElement("div");
  formHeader.className = "form-header";
  formWrapper.appendChild(formHeader);

  var formTitle = newElement("div");
  formTitle.className = "form-title";
  formTitle.innerText = "予約エリア";
  formHeader.appendChild(formTitle);

  var formContent = newElement("div");
  formContent.className = "form-content";
  formWrapper.appendChild(formContent);

  var formItem = newElement("div");
  formItem.className = "row form-item";
  formContent.appendChild(formItem);

  var labels = ["予約期間", "~", "工事番号", "工事名", "数量"];

  labels.forEach(function (label, index) {
    var groupInput = newElement("div");
    groupInput.className =
      label == "予約期間" || label == "~"
        ? "col-md-6 group-input first-row"
        : "col-md-6 group-input";
    formItem.appendChild(groupInput);

    var formLabel = newElement("label");
    formLabel.innerText = label;
    groupInput.appendChild(formLabel);

    var input = newElement("input");
    input.setAttribute("type", "text");
    if (label == "予約期間") {
      input.className = "datepicker";
      input.setAttribute("name", "start");
      input.setAttribute("placeholder", "yyyy/MM/dd");
      input.setAttribute("data-date-format", "yyyy/mm/dd");
      $(input).val(start ? start.replace(/-/g, "/") : "");
    }

    if (label == "~") {
      input.className = "datepicker";
      input.setAttribute("name", "end");
      input.setAttribute("placeholder", "yyyy/MM/dd");
      input.setAttribute("data-date-format", "yyyy/mm/dd");
      $(input).val(end ? end.replace(/-/g, "/") : "");
    }

    if (label == "工事番号") {
      input = newElement("select");
      input.setAttribute("name", "code");
      input.setAttribute(
        "class",
        "modal_construction_code_input select2 form-control modal_construction_select2 first-row"
      );

      let option = newElement("option");
      input.add(option);

      if (constructions) {
        constructions.forEach((construction) => {
          let option = newElement("option");
          option.text = construction.code;
          option.value = construction.code;

          if (construction.code == code) {
            option.selected = true;
            option.defaultSelected = true;
          }

          input.add(option);
        });
      }
    }

    if (label == "工事名") {
      input.setAttribute("name", "name");
      input.setAttribute("class", "modal_construction_name_input first-row");
      $(input).val(name);
    }

    if (label == "数量") {
      input.setAttribute("name", "quantity");
      input.setAttribute("class", "quantity");

      if (fixQuantity) {
        input.setAttribute("disabled", true);
        $(input).val(fixQuantity);
      } else {
        $(input).val(quantity);
      }
    }
    groupInput.appendChild(input);
  });

  // thêm input để edit
  let input = newElement("input");
  input.setAttribute("type", "text");
  input.setAttribute("name", "bill_type");
  input.setAttribute("style", "display:none;");
  $(input).val(billType);
  formItem.appendChild(input);

  input = newElement("input");
  input.setAttribute("type", "text");
  input.setAttribute("name", "id");
  input.setAttribute("style", "display:none;");
  $(input).val(billId);
  formItem.appendChild(input);

  input = newElement("input");
  input.setAttribute("type", "text");
  input.setAttribute("name", "product_id");
  input.setAttribute("style", "display:none;");
  $(input).val(productId);
  formItem.appendChild(input);

  $(".modal-body").append(contentDiv);
  makeDatePicker(".datepicker");
}

function removeItem(e) {
  e.preventDefault();

  var actionType = $("#modalBooking").attr("action-type");
  var item = $(e.target).closest(".content");

  if (actionType == "create") {
    checkedData = arrayRemove(checkedData, item.data("item"));
    $("#modalBooking").data("list_product", checkedData);
    localStorage.setItem("userId", JSON.stringify(checkedData));

    $(".calendar-table")
      .find(".product-name")
      .map((index, product) => {
        if (product.innerText == item.data("item")) {
          $(product).closest("tr").find("input").prop("checked", false);
        }
      });
  } else {
    $("#table-body")
      .find(".id")
      .map((index, billID) => {
        if (billID.innerText == item.data("billId")) {
          $(billID).closest("tr").find("input").prop("checked", false);
        }
      });
  }

  item.fadeOut(300, function () {
    $(this).remove();
  });
}

///// Create new html element /////
function newElement(tagName) {
  return document.createElement(tagName);
}

function makeDatePicker(element) {
  $(element)
    .datepicker({
      format: "yyyy/mm/dd",
      forceParse: false,
      autoclose: true,
    })
    .on("show.bs.modal", function (event) {
      // prevent datepicker from firing bootstrap modal "show.bs.modal"
      event.stopPropagation();
    });
}

///// Remove element in array /////,
function arrayRemove(arr, value) {
  return arr.filter(function (ele) {
    return ele != value;
  });
}
